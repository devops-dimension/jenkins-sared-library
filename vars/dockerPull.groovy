import com.bin.Docker

def call(String imageName) {
    return new Docker(this).pullDockerImage(imageName)
}
