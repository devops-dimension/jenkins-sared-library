package com.bin

class Docker implements Serializable {

    def script

    Docker(script) {
        this.script = script
    }

    def pullDockerImage(String imageName) {
        script.echo(message: '============== docker pull ==================')
        script.sh "docker pull $imageName"
    }

    def buildDockerImage(String imageName) {
        script.echo(message: '============== docker building the docker images ==================')
        script.sh "docker build -t $imageName ."
    }

    def dockerLogin() {
        script.withCredentials([script.usernamePassword(credentialsId: 'ci-docker', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
            script.echo(message: '============== docker login ==================')
            script.sh "echo $script.PASSWORD | docker login -u $script.USERNAME --password-stdin"
            /* docker login -u $USERNAME -p $PASSWORD */
        }
    }

    def dockerPush(String imageName) {
        script.echo(message: '============== docker push ==================')
        script.sh "docker push $imageName"
    }

    def dockerCleanImage() {
        script.echo(message: '============== docker clean ==================')
        script.sh 'docker system prune -af'
    }

}
