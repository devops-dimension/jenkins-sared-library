package com.bin

class Deploy implements Serializable {

    def script

    Deploy(script) {
        this.script = script
    }

    def artifactbuild() {
        script.echo(message: '"================ START BUILD! ================"')
        script.sh './build.sh'
    }

    def deployapp() {
        script.echo(message: '"================ START DEPLOY! ================"')
        script.sh 'ssh ubuntu@ec2 hostnamectl'
        script.sh './deployapp.sh'
    }

    def buildDockerCompose() {
        script.echo(message: '"================ START COMPOSE! ================"')
        script.sh 'ssh ubuntu@ec2 docker restart webserver nodejs'
        script.sh 'ssh ubuntu@ec2 docker logs webserver'
    }
}
